set tabstop=2 softtabstop=2
set shiftwidth=2
set expandtab
set smartindent

set guicursor=
set nu
set nohlsearch
set hidden

set noerrorbells

set noswapfile
set nobackup
set undodir=~/.config/nvim/undodir
set undofile

set incsearch
set termguicolors background=dark

set scrolloff=8

set signcolumn=yes
set colorcolumn=80

" Nice menu when typing `:find *.py`
set wildmode=longest,list,full
set wildmenu
" Ignore files
set wildignore+=**/node_modules/*
set wildignore+=**/.git/*
set wildignore+=**/.next/*
set wildignore+=**/build/*

call plug#begin('~/.vim/plugged')
  Plug 'nvim-lua/plenary.nvim'
  Plug 'nvim-telescope/telescope.nvim'
  Plug 'folke/tokyonight.nvim', { 'branch': 'main' }
  Plug 'nvim-lualine/lualine.nvim'
  Plug 'kyazdani42/nvim-web-devicons'
"  Plug 'EdenEast/nightfox.nvim'
"  Plug 'pbrisbin/vim-colors-off'
call plug#end()

:colorscheme tokyonight
"let g:colors_off_a_little = 1

let mapleader = " "

nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fe <cmd>Telescope file_browser<cr>

fun! TrimWhitespace()
  let l:save = winsaveview()
  keeppatterns %s/\s\+$//e
  call winrestview(l:save)
endfun

augroup JOEL
  autocmd!
  autocmd BufWritePre * :call TrimWhitespace()
augroup END
