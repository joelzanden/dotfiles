# "Advanced tab completion"
autoload -Uz compinit
if [ $(date +'%j') != $(stat -f '%Sm' -t '%j' ~/.zcompdump) ]; then
  compinit
else
  compinit -C
fi

# Vim key-bindings in command line editing and history
#bindkey -v

# From zsh.sourceforge.net **************************************************************
# " Autocorrect"
# unsetopt correct

# unsetopt correctall

# Prevent overwriting a file
setopt noclobber

# From https://www.viget.com/articles/zsh-config-productivity-plugins-for-mac-oss-default-shell/ *********

[ -z "$HISTFILE" ] && HISTFILE="${ZDOTDIR:-$HOME}/.zsh_history"

HISTSIZE=50000
SAVEHIST=10000
setopt extended_history
setopt hist_expire_dups_first
setopt hist_ignore_dups
setopt hist_ignore_space
setopt inc_append_history
setopt share_history

# Changing directories
setopt auto_cd
setopt auto_pushd
unsetopt pushd_ignore_dups
setopt pushdminus

# Completion
setopt auto_menu
setopt always_to_end
setopt complete_in_word
unsetopt flow_control
unsetopt menu_complete
zstyle ':completion:*:*:*:*:*' menu select
zstyle ':completion:*' matcher-list 'm:{a-zA-Z-_}={A-Za-z_-}' 'r:|=*' 'l:|=* r:|=*'
zstyle ':completion::complete:*' use-cache 1
zstyle ':completion::complete:*' cache-path $ZSH_CACHE_DIR
zstyle ':completion:*' list-colors ''
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#) ([0-9a-z-]#)*=01;34=0=01'

# Other
setopt prompt_subst

source ~/.paths

# Plugins with Antibody
source <(antibody init)
antibody bundle <"${ZDOTDIR:-$HOME}/.zsh_plugins"

#zsh-history-substring-search key bindings
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

# ****************************************************************************************
source ~/.aliases
source ~/.functions
source ~/.profile

fpath=(~/.zsh $fpath)
autoload -Uz compinit
compinit -u

if type brew &>/dev/null; then
  FPATH=$(brew --prefix)/share/zsh-completions:$FPATH

  autoload -Uz compinit
  compinit
fi
