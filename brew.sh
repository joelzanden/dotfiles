#!/usr/bin/env bash

brew install bash
brew install bash-completion@2

brew install neovim
brew install tmux
brew install git
brew install antibody
brew install gibo

brew install --cask bitwarden
brew install --cask karabiner-elements
brew install --cask visual-studio-code
brew install --cask firefox
brew install --cask alt-tab
brew install --cask github

brew cleanup
